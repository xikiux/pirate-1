# Pirate-1


This is a compiler and programming language designed for pirate themed fun.

The compiler is both serious and silly.
The compiler first will give a reason why your ship failed to reach it's destination.
Then you will be given the formal error.

## All stages of compilation are:
* Loading
* Lexing
* Typing
* Parsing
* Abstracting
* Intermediate Coding
* Assembling

(This is a reminder to the developers about what each stage does).

### Loading
This stage of compilation is responsible for loading files into UTF-8 strings.

### Lexing
This stage of compilation is responsible for turning files into lexemes.

Instead of calling each individual part of code "tokens" I call them "lexemes".

### Typing
This stage of compilation is responsible for taking each lexeme and giving it a more specific function.

For example, if you had a left parenthesis, what is it exactly?

Is it a left parenthesis for a function call or a left parenthesis for an argument list?

Typing de-mystifies issues like these.

### Parsing
This stage of compilation is responsible for creating a parse tree.

### Abstracting
This stage of compilation is responsible for creating an abstract syntax tree.

Or as I like to call them "Functional Trees".

Because they really have nothing to do with syntax and everything to do with program operation.

### Intermediate Coding
This stage of compilation is responsible for turning the Functional Tree into a series of architecture independent instructions.

### Assembling
This stage of compilation is responsible for taking the Intermediate Code and turning it into architecture specific assembly code.