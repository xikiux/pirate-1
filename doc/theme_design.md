# Theme Design

* Since Pirate-1 is themed around pirates, variable types, statements, structure types and more are also pirate / nautically themed.

Variables are your ships crew.
The order goes as such:
Integers:
* 8  bits - mate
* 16 bits - gunner
* 32 bits - navigator
* 64 bits - captain

Floats:
* 32 bits - carpenter
* 64 bits - quartermaster

Structures:
* struct - galleon
* class  - manowar
* module - fleet

Other:
* bool   - helmsman
* string - roster


Orders are functions.
Instead of calling functions functions. They are called orders like ordering a crew around deck.
Example:
> order sail()(navigator out) {
> avast(0);
> }

Description:
* Sail is the equivalent of a main function in C. With the exception of having multiple inputs AND outputs.
* Inputs and outputs are called arrrguments. Yes, that is spelled correctly.
* Avast is the equivalent of return in C except it is styled like a function call.