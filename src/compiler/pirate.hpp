#pragma once

#include <iostream>
#include <vector>

#include "stages/loading.hpp"
#include "stages/lexing.hpp"
#include "stages/typing.hpp"

#include "types.hpp"

namespace pirate {
    class compiler {
    public:
        types::data* compile(std::string root_file_path) {
            loading::loader loader = loading::loader();
            lexing::lexer lexer = lexing::lexer();
            typing::typer typer = typing::typer();
            types::data* output = new types::data();

            // loading
            output->add_file(new types::file());
            output->get_file(0)->p_file_address = root_file_path;
            output->get_file(0)->p_file_text = loader.load_utf_8_file(output->get_file(0)->p_file_address, output->p_errors, root_file_path);

            if (output->p_errors->has_error()) {
                return output;
            }

            // lexing
            output->get_file(0)->p_lexemes = lexer.lex(output->get_file(0)->p_file_text, output->p_errors, output->get_file(0)->p_file_address);

            if (output->p_errors->has_error()) {
                return output;
            }

            // typing
            output->get_file(0)->p_typelings = typer.type(output->get_file(0)->p_lexemes, output->p_errors, output->get_file(0)->p_file_address);

            if (output->p_errors->has_error()) {
                return output;
            }

            return output;
        }
    };
}