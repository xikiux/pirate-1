#pragma once

#include <iostream>
#include <vector>

#include "../types.hpp"

namespace pirate::lexing {
    class lexer {
    public:
        types::lexemes* lex(types::string* file, types::errors* errors, std::string file_path) {
            types::lexemes* output = new types::lexemes();
            types::string* temp;
            unsigned long long inc;

            output->add(new types::lexeme(types::lt::lt_bof, types::string("bof", errors, file_path)));

            // get assumed types
            for (unsigned long long i = 0; i < file->length(); i += inc) {
                inc = 1;

                // whitespace
                if (file->get(i).p_raw[0] == ' ' || file->get(i).p_raw[0] == '\t' || file->get(i).p_raw[0] == '\n' || file->get(i).p_raw[0] == '\r') {
                    continue;
                }

                // brackets
                if (file->get(i).p_raw[0] == '(') {
                    output->add(new types::lexeme(types::lt::lt_lp, types::string("(", errors, file_path)));
                    continue;
                }
                if (file->get(i).p_raw[0] == ')') {
                    output->add(new types::lexeme(types::lt::lt_rp, types::string(")", errors, file_path)));
                    continue;
                }
                if (file->get(i).p_raw[0] == '{') {
                    output->add(new types::lexeme(types::lt::lt_lcb, types::string("{", errors, file_path)));
                    continue;
                }
                if (file->get(i).p_raw[0] == '}') {
                    output->add(new types::lexeme(types::lt::lt_rcb, types::string("}", errors, file_path)));
                    continue;
                }

                // statement pieces
                if (file->get(i).p_raw[0] == ',') {
                    output->add(new types::lexeme(types::lt::lt_comma, types::string(",", errors, file_path)));
                    continue;
                }
                if (file->get(i).p_raw[0] == ';') {
                    output->add(new types::lexeme(types::lt::lt_semi_colon, types::string(";", errors, file_path)));
                    continue;
                }

                // flexibles
                if ((temp = word(file, i)) != 0) {
                    output->add(new types::lexeme(types::lt::lt_word, *temp));
                    inc = temp->length();
                    continue;
                }
                if ((temp = integer_literal(file, i)) != 0) {
                    output->add(new types::lexeme(types::lt::lt_integer_literal, *temp));
                    inc = temp->length();
                    continue;
                }

                errors->create_error(new types::error(types::et::et_lexing, types::set::set_lexing__no_found_lexeme, "No lexeme could be found!", file_path));
                break;
            }

            output->add(new types::lexeme(types::lt::lt_eof, types::string("eof", errors, file_path)));

            return output;
        }

    private:
		types::string* word(types::string* file, unsigned long long index) {
			if (std::string("abcdefghijklmnopqrstuvwxyz_ABCDEFGHIJKLMNOPQRSTUVWXYZ").find(file->get(index).p_raw[0]) != std::string::npos) {
				types::string* output = new types::string();
                unsigned long long length = 1;

                // get string
                output->add(file->get(index));
				while (index + length < file->length() && std::string("abcdefghijklmnopqrstuvwxyz_ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789").find(file->get(index + length).p_raw[0]) != std::string::npos) {
                    output->add(file->get(index + length));
                    length++;
				}

				return output;
			} else {
				return 0;
			}
		}

		types::string* integer_literal(types::string* file, unsigned long long index) {
            types::string* output = new types::string();
			unsigned long long length = 0;

			while (index + length < file->length() && std::string("0123456789").find(file->get(index + length).p_raw[0]) != std::string::npos) {
				output->add(file->get(index + length));
                length++;
			}

            if (length > 0) {
			    return output;
            } else {
                delete output;
                return 0;
            }
		}
    };
}
