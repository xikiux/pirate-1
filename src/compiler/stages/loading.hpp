#pragma once

#include <iostream>
#include <fstream>
#include <vector>

#include "../types.hpp"

namespace pirate::loading {
    class loader {
    public:
        types::string* load_utf_8_file(std::string subject_file_path, types::errors* errors, std::string origin_file_path) {
            types::string* output;
            std::string raw = load_file(subject_file_path, errors, origin_file_path);

            if (errors->has_error()) {
                return 0;
            }

            output = new types::string();

            for (unsigned long long i = 0; i < raw.length() && errors->has_error() == false; i += (unsigned long long)output->get(output->length() - 1).p_length) {
                output->add(types::utf_8_char((unsigned char*)raw.c_str(), i, errors, origin_file_path));
            }

            return output;
        }

    private:
        std::string load_file(std::string file_path, types::errors* errors, std::string origin_file_path) {
            std::ifstream f(file_path);
    
            if (f.good()) {
                std::string output = std::string().assign(std::istreambuf_iterator<char>(f), std::istreambuf_iterator<char>());

                if (output == "") {
                    errors->create_error(new types::error(types::et::et_loading, types::set::set_loading__empty_file, "File " + file_path + " was found but is empty!", origin_file_path));
                }

                return output;
            } else {
                errors->create_error(new types::error(types::et::et_loading, types::set::set_loading__invalid_file_path, "File " + file_path + " could not be found!", origin_file_path));
                return "";
            }
        }
    };
}