#pragma once

#include <iostream>
#include <vector>

#include "../types.hpp"

namespace pirate::typing {
    class typer {
    public:
        types::typelings* type(types::lexemes* lexemes, types::errors* errors, std::string file_path) {
            types::typelings* output = new types::typelings();
            unsigned long long index = 0;

            type__file(output, lexemes, &index, errors, file_path);

            return output;
        }

    private:
        inline bool can_look_ahead(types::lexemes* lexemes, unsigned long long* index, unsigned long long look_ahead) {
            return *index <= lexemes->count() - 1 - look_ahead;
        }

        void type__file(types::typelings* typelings, types::lexemes* lexemes, unsigned long long* index, types::errors* errors, std::string& file_path) {
            // get file beginning lexeme
            if (can_look_ahead(lexemes, index, 0) && lexemes->get(*index)->type() == types::lt::lt_bof) {
                typelings->add(new types::typeling(types::tt::tt_file_opener, lexemes->get(*index)));
                *index += 1;
            } else {
                errors->create_error(new types::error(types::et::et_typing, types::set::set_unknown, "Missing bof.", file_path));
                return;
            }

            // get file contents
            if (can_look_ahead(lexemes, index, 0) && lexemes->get(*index)->type() != types::lt::lt_eof) {
                if (lexemes->get(*index)->type() == types::lt::lt_word) {
                    type__global_declaration(typelings, lexemes, index, errors, file_path);
                } else {
                    errors->create_error(new types::error(types::et::et_typing, types::set::set_unknown, "Unknown definition type: " + std::to_string(*index - lexemes->count() - 1), file_path));
                    return;
                }
            } else {
                errors->create_error(new types::error(types::et::et_typing, types::set::set_unknown, "Missing file contents.", file_path));
                return;
            }
            if (errors->has_error()) {
                return;
            }

            // get file end lexeme
            if (can_look_ahead(lexemes, index, 0) && lexemes->get(*index)->type() == types::lt::lt_eof) {
                typelings->add(new types::typeling(types::tt::tt_file_closer, lexemes->get(*index)));
                *index += 1;
            } else {
                errors->create_error(new types::error(types::et::et_typing, types::set::set_unknown, "Missing eof.", file_path));
                return;
            }

            return;
        }

        void type__global_declaration(types::typelings* typelings, types::lexemes* lexemes, unsigned long long* index, types::errors* errors, std::string& file_path) {
            // get declaration type
            if (lexemes->get(*index)->value().std__string() == "order") {
                typelings->add(new types::typeling(types::tt::tt_declaration_type_name, lexemes->get(*index)));
                *index += 1;
                type__function(typelings, lexemes, index, errors, file_path);
            } else {
                errors->create_error(new types::error(types::et::et_typing, types::set::set_unknown, "Unsupported structure: " + std::to_string(*index - lexemes->count() - 2), file_path));
                return;
            }

            return;
        }

        void type__function(types::typelings* typelings, types::lexemes* lexemes, unsigned long long* index, types::errors* errors, std::string& file_path) {
            // the keyword 'order' is ALWAYS checked to know what to type. So we can skip this check.

            // get function name
            if (can_look_ahead(lexemes, index, 0) && lexemes->get(*index)->type() == types::lt::lt_word) {
                typelings->add(new types::typeling(types::tt::tt_declaration_name, lexemes->get(*index)));
                *index += 1;
            } else {
                errors->create_error(new types::error(types::et::et_typing, types::set::set_unknown, "Missing function name.", file_path));
                return;
            }

            // get function inputs
            type__argument_definition_list(typelings, lexemes, index, errors, file_path);
            if (errors->has_error()) {
                return;
            }
            // get function outputs
            type__argument_definition_list(typelings, lexemes, index, errors, file_path);
            if (errors->has_error()) {
                return;
            }

            // get function scope
            type__statement_scope(typelings, lexemes, index, errors, file_path);

            return;
        }

        void type__argument_definition_list(types::typelings* typelings, types::lexemes* lexemes, unsigned long long* index, types::errors* errors, std::string& file_path) {
            // get arguments opener
            if (can_look_ahead(lexemes, index, 0) && lexemes->get(*index)->type() == types::lt::lt_lp) {
                typelings->add(new types::typeling(types::tt::tt_declaration_arguments_opener, lexemes->get(*index)));
                *index += 1;
            } else {
                errors->create_error(new types::error(types::et::et_typing, types::set::set_unknown, "Missing declaration argument list opener.", file_path));
                return;
            }

            // check for empty arguments
            if (can_look_ahead(lexemes, index, 0) && lexemes->get(*index)->type() == types::lt::lt_rp) {
                typelings->add(new types::typeling(types::tt::tt_declaration_arguments_closer, lexemes->get(*index)));
                *index += 1;
            // arguments are not empty
            } else {
                while (can_look_ahead(lexemes, index, 0)) {
                    // get argument
                    type__variable_stem(typelings, lexemes, index, errors, file_path);
                    if (errors->has_error()) {
                        return;
                    }

                    // check for continuation, argument list close or error
                    if (can_look_ahead(lexemes, index, 0)) {
                        if (lexemes->get(*index)->type() == types::lt::lt_comma) {
                            typelings->add(new types::typeling(types::tt::tt_declaration_arguments_continuer, lexemes->get(*index)));
                            *index += 1;
                        } else if (lexemes->get(*index)->type() == types::lt::lt_rp) {
                            typelings->add(new types::typeling(types::tt::tt_declaration_arguments_closer, lexemes->get(*index)));
                            *index += 1;
                            return;
                        } else {
                            break;
                        }
                    }
                }

                errors->create_error(new types::error(types::et::et_typing, types::set::set_unknown, "Missing declaration argument list continuer / closer.", file_path));
                return;
            }

            return;
        }

        void type__variable_stem(types::typelings* typelings, types::lexemes* lexemes, unsigned long long* index, types::errors* errors, std::string& file_path) {
            // get variable type
            if (can_look_ahead(lexemes, index, 0) && lexemes->get(*index)->type() == types::lt::lt_word) {
                typelings->add(new types::typeling(types::tt::tt_declaration_type_name, lexemes->get(*index)));
                *index += 1;
            } else {
                errors->create_error(new types::error(types::et::et_typing, types::set::set_unknown, "Missing declaration type name.", file_path));
                return;
            }

            // get declaration name
            if (can_look_ahead(lexemes, index, 0) && lexemes->get(*index)->type() == types::lt::lt_word) {
                typelings->add(new types::typeling(types::tt::tt_declaration_name, lexemes->get(*index)));
                *index += 1;
            } else {
                errors->create_error(new types::error(types::et::et_typing, types::set::set_unknown, "Missing declaration name.", file_path));
                return;
            }

            return;
        }

        void type__statement_scope(types::typelings* typelings, types::lexemes* lexemes, unsigned long long* index, types::errors* errors, std::string& file_path) {
            // get scope opener
            if (can_look_ahead(lexemes, index, 0) && lexemes->get(*index)->type() == types::lt::lt_lcb) {
                typelings->add(new types::typeling(types::tt::tt_scope_opener, lexemes->get(*index)));
                *index += 1;
            } else {
                errors->create_error(new types::error(types::et::et_typing, types::set::set_unknown, "Missing scope opener.", file_path));
                return;
            }

            // check for empty scope
            if (can_look_ahead(lexemes, index, 0) && lexemes->get(*index)->type() == types::lt::lt_rcb) {
                typelings->add(new types::typeling(types::tt::tt_scope_closer, lexemes->get(*index)));
                *index += 1;
            // scope is not empty
            } else {
                while (can_look_ahead(lexemes, index, 0)) {
                    type__statement(typelings, lexemes, index, errors, file_path);
                    if (errors->has_error()) {
                        return;
                    }

                    if (can_look_ahead(lexemes, index, 0)) {
                        if (lexemes->get(*index)->type() == types::lt::lt_rcb) {
                            typelings->add(new types::typeling(types::tt::tt_scope_closer, lexemes->get(*index)));
                            *index += 1;
                            return;
                        } else if (lexemes->get(*index)->type() == types::lt::lt_word) {
                            continue;
                        } else {
                            errors->create_error(new types::error(types::et::et_typing, types::set::set_unknown, "Missing closing scope / statement start.", file_path));
                            return;
                        }
                    } else {
                        break;
                    }
                }

                errors->create_error(new types::error(types::et::et_typing, types::set::set_unknown, "Missing scope closer.", file_path));
                return;
            }

            return;
        }

        void type__statement(types::typelings* typelings, types::lexemes* lexemes, unsigned long long* index, types::errors* errors, std::string& file_path) {
            // get statment chain opener
            if (can_look_ahead(lexemes, index, 0) && lexemes->get(*index)->type() == types::lt::lt_word) {
                typelings->add(new types::typeling(types::tt::tt_statement_chain_stem, lexemes->get(*index)));
                *index += 1;
            } else {
                errors->create_error(new types::error(types::et::et_typing, types::set::set_unknown, "Missing statement chain stem.", file_path));
                return;
            }

            // get argument pass list
            type__argument_pass_list(typelings, lexemes, index, errors, file_path);

            // get semi-colon
            if (can_look_ahead(lexemes, index, 0) && lexemes->get(*index)->type() == types::lt::lt_semi_colon) {
                typelings->add(new types::typeling(types::tt::tt_statement_chain_ender, lexemes->get(*index)));
                *index += 1;
            } else {
                errors->create_error(new types::error(types::et::et_typing, types::set::set_unknown, "Missing statement chain ender.", file_path));
                return;
            }

            return;
        }

        void type__argument_pass_list(types::typelings* typelings, types::lexemes* lexemes, unsigned long long* index, types::errors* errors, std::string& file_path) {
            // check for opening parenthesis
            if (can_look_ahead(lexemes, index, 0) && lexemes->get(*index)->type() == types::lt::lt_lp) {
                typelings->add(new types::typeling(types::tt::tt_statement_chain_function_call_opener, lexemes->get(*index)));
                *index += 1;
            } else {
                errors->create_error(new types::error(types::et::et_typing, types::set::set_unknown, "Missing argument pass list opener.", file_path));
                return;
            }

            // check for empty list
            if (can_look_ahead(lexemes, index, 0) && lexemes->get(*index)->type() == types::lt::lt_rp) {
                typelings->add(new types::typeling(types::tt::tt_statement_chain_function_call_closer, lexemes->get(*index)));
                *index += 1;
            // list is not empty
            } else {
                while (can_look_ahead(lexemes, index, 0)) {
                    // check if there is a comma for an empty argument orcheck for an expression
                    if (lexemes->get(*index)->type() == types::lt::lt_comma) {
                        typelings->add(new types::typeling(types::tt::tt_statement_chain_function_call_continuer, lexemes->get(*index)));
                        *index += 1;
                    } else {
                        type__expression(typelings, lexemes, index, errors, file_path);
                        if (errors->has_error()) {
                            return;
                        }
                    }

                    // check for a list end or continuer
                    if (can_look_ahead(lexemes, index, 0)) {
                        if (lexemes->get(*index)->type() == types::lt::lt_rp) {
                            typelings->add(new types::typeling(types::tt::tt_statement_chain_function_call_closer, lexemes->get(*index)));
                            *index += 1;
                            return;
                        } else if (lexemes->get(*index)->type() == types::lt::lt_comma) {
                            typelings->add(new types::typeling(types::tt::tt_statement_chain_function_call_continuer, lexemes->get(*index)));
                            *index += 1;
                            continue;
                        } else {
                            errors->create_error(new types::error(types::et::et_typing, types::set::set_unknown, "Missing argument pass list end / continuer.", file_path));
                            return;
                        }
                    } else {
                        break;
                    }
                }

                errors->create_error(new types::error(types::et::et_typing, types::set::set_unknown, "Missing argument list closer.", file_path));
                return;
            }

            return;
        }

        void type__expression(types::typelings* typelings, types::lexemes* lexemes, unsigned long long* index, types::errors* errors, std::string& file_path) {
            if (can_look_ahead(lexemes, index, 0) && lexemes->get(*index)->type() == types::lt::lt_integer_literal) {
                typelings->add(new types::typeling(types::tt::tt_expression_literal, lexemes->get(*index)));
                *index += 1;
            } else {
                errors->create_error(new types::error(types::et::et_typing, types::set::set_unknown, "Missing expression literal.", file_path));
                return;
            }

            return;
        }
    };
}