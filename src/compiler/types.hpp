#pragma once

#include <iostream>
#include <vector>

namespace pirate::types {
    // error type
    enum et {
        et_unknown,
        et_internal,
        et_loading,
        et_lexing,
        et_typing,
        et_parsing
    };

    // specific error type
    enum set {
        set_unknown,
        // loading
        set_loading__invalid_file_path,
        set_loading__empty_file,
        set_loading__invalid_utf_8_character,
        // lexing
        set_lexing__no_found_lexeme
    };

    class error {
        et m_type;
        set m_specific_type;
        std::string m_msg;
        std::string m_file_occured;

    public:
        error(et type, set specific_type, std::string msg, std::string file_occured) {
            m_type = type;
            m_specific_type = specific_type;
            m_msg = msg;
            m_file_occured = file_occured;
        }

        et type() {
            return m_type;
        }

        set specific() {
            return m_specific_type;
        };

        std::string msg() {
            return m_msg;
        }

        std::string file_occured() {
            return m_file_occured;
        }
    };

    class errors {
        error* m_error;

    public:
        errors() {
            m_error = 0;
        }

        ~errors() {
            delete m_error;
        }

        void create_error(error* e) {
            m_error = e;
        }

        bool has_error() {
            if (m_error != 0) {
                return true;
            } else {
                return false;
            }
        }

        std::string stringify() {
            return "[Error: " + m_error->msg() + "]";
        }

        error* get_error() {
            return m_error;
        }
    };

    class utf_8_char {
    public:
        unsigned int p_value;
        unsigned char p_length;
        unsigned char p_raw[7];

        utf_8_char() {
            p_value = 0;
            p_length = 0;
            p_raw[0] = 0;
            p_raw[1] = 0;
            p_raw[2] = 0;
            p_raw[3] = 0;
            p_raw[4] = 0;
            p_raw[5] = 0;
            p_raw[6] = 0; // always zero for null termination
        }

        utf_8_char(char c) {
            p_value = c;
            p_length = 1;
            p_raw[0] = c;
            p_raw[1] = 0;
            p_raw[2] = 0;
            p_raw[3] = 0;
            p_raw[4] = 0;
            p_raw[5] = 0;
            p_raw[6] = 0; // always zero for null termination
        }

        utf_8_char(unsigned char* raw_str_data, unsigned long long index, types::errors* errors, std::string file_path) {
            // pre-initialize properties to zero
            p_value = 0;
            p_length = 0;
            p_raw[0] = 0;
            p_raw[1] = 0;
            p_raw[2] = 0;
            p_raw[3] = 0;
            p_raw[4] = 0;
            p_raw[5] = 0;
            p_raw[6] = 0;
            
            if ((raw_str_data[index] & 0b11000000) == 0b10000000 && raw_str_data[index] != 0b11111111) {
                errors->create_error(new types::error(types::et::et_loading, types::set::set_loading__invalid_utf_8_character, "This file is not valid utf-8!", file_path));
                return;
            }

            // get uft-8 data
            if ((raw_str_data[index] & 0b10000000) == 0b00000000) {
                p_length = 1;

                p_value = (unsigned int)raw_str_data[index];

                p_raw[0] = raw_str_data[index];
            } else if ((raw_str_data[index] & 0b11100000) == 0b11000000) {
                p_length = 2;
                
                p_value += (unsigned int)(raw_str_data[index] & 0b00011111) * 64;
                p_value += (unsigned int)(raw_str_data[index + 1] & 0b00111111);

                p_raw[0] = raw_str_data[index];
                p_raw[1] = raw_str_data[index + 1];
            } else if ((raw_str_data[index] & 0b11110000) == 0b11100000) {
                p_length = 3;
                
                p_value += (unsigned int)(raw_str_data[index] & 0b00001111) * 64 * 64;
                p_value += (unsigned int)(raw_str_data[index + 1] & 0b00111111) * 64;
                p_value += (unsigned int)(raw_str_data[index + 2] & 0b00111111);

                p_raw[0] = raw_str_data[index];
                p_raw[1] = raw_str_data[index + 1];
                p_raw[2] = raw_str_data[index + 2];
            } else if ((raw_str_data[index] & 0b11111000) == 0b11110000) {
                p_length = 4;

                p_value += (unsigned int)(raw_str_data[index] & 0b00000111) * 64 * 64 * 64;
                p_value += (unsigned int)(raw_str_data[index + 1] & 0b00111111) * 64 * 64;
                p_value += (unsigned int)(raw_str_data[index + 2] & 0b00111111) * 64;
                p_value += (unsigned int)(raw_str_data[index + 3] & 0b00111111);

                p_raw[0] = raw_str_data[index];
                p_raw[1] = raw_str_data[index + 1];
                p_raw[2] = raw_str_data[index + 2];
                p_raw[3] = raw_str_data[index + 3];
            } else if ((raw_str_data[index] & 0b11111100) == 0b11111000) {
                p_length = 5;

                p_value += (unsigned int)(raw_str_data[index] & 0b00000011) * 64 * 64 * 64 * 64;
                p_value += (unsigned int)(raw_str_data[index + 1] & 0b00111111) * 64 * 64 * 64;
                p_value += (unsigned int)(raw_str_data[index + 2] & 0b00111111) * 64 * 64;
                p_value += (unsigned int)(raw_str_data[index + 3] & 0b00111111) * 64;
                p_value += (unsigned int)(raw_str_data[index + 4] & 0b00111111);

                p_raw[0] = raw_str_data[index];
                p_raw[1] = raw_str_data[index + 1];
                p_raw[2] = raw_str_data[index + 2];
                p_raw[3] = raw_str_data[index + 3];
                p_raw[4] = raw_str_data[index + 4];
            } else if ((raw_str_data[index] & 0b11111110) == 0b11111100) {
                p_length = 5;
                
                p_value += (unsigned int)(raw_str_data[index] & 0b00000011) * 64 * 64 * 64 * 64 * 64;
                p_value += (unsigned int)(raw_str_data[index + 1] & 0b00111111) * 64 * 64 * 64 * 64;
                p_value += (unsigned int)(raw_str_data[index + 2] & 0b00111111) * 64 * 64 * 64;
                p_value += (unsigned int)(raw_str_data[index + 3] & 0b00111111) * 64 * 64;
                p_value += (unsigned int)(raw_str_data[index + 4] & 0b00111111) * 64;
                p_value += (unsigned int)(raw_str_data[index + 5] & 0b00111111);

                p_raw[0] = raw_str_data[index];
                p_raw[1] = raw_str_data[index + 1];
                p_raw[2] = raw_str_data[index + 2];
                p_raw[3] = raw_str_data[index + 3];
                p_raw[4] = raw_str_data[index + 4];
                p_raw[5] = raw_str_data[index + 5];
            } else {
                errors->create_error(new types::error(types::et::et_loading, types::set::set_loading__invalid_utf_8_character, "This file is not valid utf-8!", file_path));
            }
        }
    };

    class string {
        std::vector<utf_8_char> m_string;

    public:
        string() {
            m_string = std::vector<utf_8_char>();
        }

        string(std::string cpp_string, types::errors* errors, std::string file_path) {
            m_string = std::vector<utf_8_char>();
            unsigned long long l = 0;

            while (l < cpp_string.length()) {
                add(types::utf_8_char((unsigned char*)cpp_string.c_str(), l, errors, file_path));
                l += get(length() - 1).p_length;
            }
        }

        void add(utf_8_char c) {
            m_string.push_back(c);
        }

        unsigned long long length() {
            return m_string.size();
        }

        unsigned long long raw_length() {
            unsigned long long count = 0;

            for (unsigned long long i = 0; i < m_string.size(); i++) {
                count += m_string[i].p_length;
            }

            return count;
        }

        std::string std__string() {
            std::string output = "";

            for (unsigned long long i = 0; i < length(); i++) {
                for (unsigned long long j = 0; j < m_string[i].p_length; j++) {
                    output += m_string[i].p_raw[j];
                }
            }

            return output;
        }

        utf_8_char get(unsigned long long index) {
            return m_string[index];
        }

        utf_8_char operator[](unsigned long long index) {
            return m_string[index];
        }

        string sub(unsigned long long index, unsigned long long length) {
            string output = string();

            for (unsigned long long i = 0; i < length; i++) {
                output.add(m_string[index + i]);
            }

            return output;
        }
    };

    enum lt {
        lt_unknown,
        lt_bof,
        lt_eof,
        lt_word,
        lt_integer_literal,
        lt_comma,
        lt_equals,
        lt_semi_colon,
        lt_lp,
        lt_rp,
        lt_lsb,
        lt_rsb,
        lt_lcb,
        lt_rcb,
    };

    class lexeme {
        lt m_type;
        string m_value;

    public:
        lexeme(lt type, string value) {
            m_type = type;
            m_value = value;
        }

        lt type() {
            return m_type;
        }

        string value() {
            return m_value;
        }

        std::string stringify() {
            return std::to_string(m_type) + " : " +  m_value.std__string();
        }
    };

    class lexemes {
        std::vector<lexeme*> m_lexemes;

    public:
        lexemes() {
            m_lexemes = std::vector<lexeme*>();
        }

        ~lexemes() {
            for (auto m : m_lexemes) {
                delete m;
            }
        }

        void add(lexeme* l) {
            m_lexemes.push_back(l);
        }

        unsigned long long count() {
            return m_lexemes.size();
        }

        lexeme* get(unsigned long long index) {
            return m_lexemes[index];
        }

        lexeme* get_last() {
            return m_lexemes[m_lexemes.size() - 1];
        }

        std::string stringify() {
            std::string output = "Lexemes:\n";

            for (auto m : m_lexemes) {
                output += m->stringify() + "\n";
            }

            return output;
        }
    };

    // typeling type
    enum tt {
        tt_unknown,
        // files
        tt_file_opener,
        tt_file_closer,
        // declarations
        tt_declaration_type_name,
        tt_declaration_name,
        tt_declaration_arguments_opener,
        tt_declaration_arguments_closer,
        tt_declaration_arguments_continuer,
        // scopes
        tt_scope_opener,
        tt_scope_closer,
        // statements
        tt_statement_chain_stem,
        tt_statement_chain_function_call_opener,
        tt_statement_chain_function_call_closer,
        tt_statement_chain_function_call_continuer,
        tt_statement_chain_ender,
        // expressions
        tt_expression_literal,
    };

    class typeling {
    public:
        tt p_type_data;
        lexeme* p_lexeme; // do not delete in this object!

        typeling(tt type_data, lexeme* lexeme) {
            p_type_data = type_data;
            p_lexeme = lexeme;
        }

        std::string stringify() {
            return std::to_string(p_type_data) + " : " + p_lexeme->value().std__string();
        }
    };

    class typelings {
        std::vector<typeling*> m_typelings;
    
    public:
        typelings() {
            m_typelings = std::vector<typeling*>();
        }

        ~typelings() {
            for (auto m : m_typelings) {
                delete m;
            }
        }

        void add(typeling* t) {
            m_typelings.push_back(t);
        }

        unsigned long long count() {
            return m_typelings.size();
        }

        typeling* get(unsigned long long index) {
            return m_typelings[index];
        }

        std::string stringify() {
            std::string output = "Typelings:\n";

            for (unsigned long long i = 0; i < count(); i++) {
                output += get(i)->stringify() + "\n";
            }

            return output;
        }
    };

    // parsling type
    enum pt {
        pt_unknown,
        pt_file,
        pt_function,
        pt_function_declaration,
        pt_function_scope,

    };

    class parsling {
        uint64_t m_integer_value;
        std::vector<parsling*> m_sub_parslings;

    public:
        parsling() {
            m_integer_value = 0;
            m_sub_parslings = std::vector<parsling*>();
        }
    };

    class file {
    public:
        std::string p_file_address;
        string* p_file_text;
        lexemes* p_lexemes;
        typelings* p_typelings;

        file() {
            p_file_text = 0;
            p_lexemes = 0;
            p_typelings = 0;
        }

        ~file() {
            if (p_file_text != 0) {
                delete p_file_text;
            }
            if (p_lexemes != 0) {
                delete p_lexemes;
            }
            if (p_typelings != 0) {
                delete p_typelings;
            }
        }
    };

    class data {
        std::vector<file*> m_files;

    public:
        errors* p_errors;

        data() {
            m_files = std::vector<file*>();
            p_errors = new errors();
        }

        ~data() {
            for (auto m : m_files) {
                delete m;
            }

            delete p_errors;
        }

        unsigned long long add_file(file* f) {
            m_files.push_back(f);
            return file_count() - 1;
        }

        unsigned long long file_count() {
            return m_files.size();
        }

        file* get_file(unsigned long long file_ID) {
            return m_files[file_ID];
        }
    };
}