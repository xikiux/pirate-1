#include <iostream>

#include "compiler/pirate.hpp"

int main() {
    printf("Arrrgh!\n");
    fflush(stdout);

    pirate::compiler c = pirate::compiler();

    pirate::types::data* d = c.compile("prog/basic.ship");

    if (d->p_errors->has_error()) {
        std::cout << d->p_errors->stringify() << std::endl;
    }

    std::cout << d->get_file(0)->p_lexemes->stringify() << std::endl << d->get_file(0)->p_typelings->stringify() << std::endl;

    delete d;

    return 0;
}